
export interface productData {
      productTitle: string;
      productPrice: string;
      productDesc: string;
      seller: string;
      productId: string;
}

export interface cartData {
  productTitle: string;
  productPrice: number;
}
