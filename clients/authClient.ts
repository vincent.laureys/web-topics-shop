import {db, auth} from "@/firebase";
import {signInWithEmailAndPassword, createUserWithEmailAndPassword, signOut} from "@firebase/auth";
import {setDoc, doc } from '@firebase/firestore'
import {checkRank} from '~/clients/dataClient';

export async function login(email: string, password: string): Promise<void> {
  signInWithEmailAndPassword(auth, email, password).then(
    (userCredential: any) => {
      const user = userCredential.user;
      localStorage.setItem("uid", user.uid);
    }
  ). catch((error: any) => {
    console.log('login error', error);
  })
  await checkRank();
}

export async function registerUser(email: string, password: string, username: string, address: string): Promise<void> {
  const userInfo = {
    username: username,
    address: address,
    rank: "client",
  }
  createUserWithEmailAndPassword(auth, email, password).then(
     (userCredential: any) => {
       setDoc(doc(db, 'Users', userCredential.user.uid), userInfo);
       localStorage.setItem("uid", userCredential.user.uid);
    }
  )
    .catch((error: any) => {
      console.log(error);
    })
}

export async function registerSeller(email: string, password: string, username: string, address: string, phonenumber: string): Promise<void> {
  const sellerInfo = {
    username: username,
    address: address,
    phonenumber: phonenumber,
    rank: "Seller",
  }

  createUserWithEmailAndPassword(auth, email, password).then(
     (userCredential: any) => {
       setDoc(doc(db, 'Users', userCredential.user.uid), sellerInfo);
       localStorage.setItem("uid", userCredential.user.uid);
    }
  ).catch((error: any) => {
    console.log(error);
  })
}

export async function logout(): Promise<void> {
  signOut(auth).then(() => {
    console.log("logged out");
  }).catch((error: any) => {
    console.log(error);
  })
}
