import { db, auth } from '@/firebase';
import { setDoc, doc, getDoc, collection, addDoc, getDocs } from '@firebase/firestore';
import { cartData, productData } from '~/interfaces/interface';

export async function checkRank(): Promise<void> {
  let uid = localStorage.getItem('uid')!;
  const docSnap = await getDoc(doc(db, 'Users', uid));
  if (docSnap.exists()) {
    localStorage.setItem('rank', docSnap.data().rank);
  } else {
    console.log('No such document!');
  }
}

export async function createProduct(productTitle: string, productPrice: number, productDesc: string): Promise<void> {

  const docRef = await addDoc(collection(db, 'Products'), {
    productTitle: productTitle,
    productPrice: productPrice,
    productDesc: productDesc,
    seller: localStorage.getItem('uid')!
  });
}

export async function getProducts(): Promise<productData> {
  const data: any = [];
  const querySnapshot = await getDocs(collection(db, 'Products'));
  querySnapshot.forEach((document) => {
    let fireData = {
      'productTitle': document.data().productTitle,
      'productPrice': document.data().productPrice,
      'productDesc': document.data().productDesc,
      'seller': document.data().username,
      'productId': document.id,
    };
    data.push(fireData);
  });

  return data;
}

export async function addToCart(productId: string, productTitle: string, productPrice: string): Promise<void> {
  await setDoc(doc(db, localStorage.getItem('uid')!, productId), {
    productTitle: productTitle,
    productPrice: productPrice,
  });
}

export async function getCart(): Promise<cartData> {
    const data: any = [];
    const querySnapshot = await getDocs(collection(db, localStorage.getItem('uid')!));
    querySnapshot.forEach((document) => {
      let fireData = {
        'productTitle': document.data().productTitle,
        'productPrice': parseInt(document.data().productPrice),
      }
      data.push(fireData);
  })
  return data;
}
