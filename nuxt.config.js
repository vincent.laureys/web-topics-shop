import colors from 'vuetify/es5/util/colors'

export default {
  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s - TopicShop',
    title: 'De 3D componenten shop',
    htmlAttrs: {
      lang: 'nl'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'De beste webshop voor al jouw 3D componenten waar iedereen kan kopen en verkopen!' },
      { hid: 'keywords', name: 'keywords', content: '3D, 3D printer, webshop, 3D component, PLA-filament, kopen, verkopen, TopicShop'},
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
    '@nuxtjs/google-analytics'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/robots',
    '@nuxtjs/sitemap',
    '@nuxtjs/gtm',
    '@nuxtjs/axios',
    '@nuxtjs/proxy',
  ],
  axios: {
    proxy: false,
  },
  googleAnalytics: {
    id: 'UA-198816016-2'
  },
  gtm: {
    id: 'GTM-MDNNHXM',
  },
  sitemap: {
    hostname: 'https://main--genuine-platypus-d6a4cc.netlify.app/',
    gzip: true,
    exclude: [
      '/addProduct'
    ],
    routes: [
      '/',
      '/login',
      '/register',
    ]
  },
  robots: {
    UserAgent: '*',
    Disallow: '/addProduct',
  },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: true,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    vendor: ["axios"],
  },
  env: {
    stripePublischableKey: "pk_test_51K6f4zE0p6a4NxQtq20FE06i5T03SR68XVO78uweaqLZoKhhy8g2ye0TRFavDxWW5ckBq99RlhlQex9NX45XIKFJ00i9Gx8RyV"
  }
}
