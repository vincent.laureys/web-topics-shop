# TopicShop

TopicShop is a jamstack webshop made with Nuxtjs.

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

## Features

### `jamstack`

The webshop is made with jamstack.

### `nuxtjs`

The webshop is developed in Nuxtjs.

### `seo`

The website is seo optimised.

### `Google analytics & tag manager`

The webshop has google analytics & tag manager for tracking.

### `Stripe`

The webshop has a payment system with Stripe.

## License
[MIT](https://gitlab.com/vincent.laureys/web-topics-shop/-/blob/main/LICENSE)
