// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getFirestore } from "firebase/firestore";
import { getAuth, browserLocalPersistence, setPersistence } from "firebase/auth";
import { getStorage } from "firebase/storage";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBA8r4Eo7TAA4JtdEx7BBAT8hqWPHF_otk",
  authDomain: "web-topics-8af09.firebaseapp.com",
  projectId: "web-topics-8af09",
  storageBucket: "web-topics-8af09.appspot.com",
  messagingSenderId: "531206168349",
  appId: "1:531206168349:web:696bd869eff624da30cbf4",
  measurementId: "G-8TVFLE9H83"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);
export const auth = getAuth(app);
export const storage = getStorage(app);

setPersistence(auth, browserLocalPersistence);


